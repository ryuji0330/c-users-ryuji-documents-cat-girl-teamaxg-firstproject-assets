﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Constant;

namespace ScrollHelper {

    public static class Scroll {

        /*
        * 現在の進行方向を取得して逆方向を返却する
        * @return Int direction
        */
        public static int ChangeDirection(int direction) {
            if (direction == Scenes.SCROLL_LEFT) {
                direction = Scenes.SCROLL_RIGHT;
            } else {
                direction = Scenes.SCROLL_LEFT;
            }
            return direction;
        }


        /*
        * 現在のスピードに変化量を掛けて返却
        * @return Float speed
        */
        public static float ChangeSpeed(float speed, float addSpeed) {
            speed = speed * addSpeed;
            return speed;
        }


        /*
        * オブジェクトの名前からゲームオブジェクトを返却
        * @return GameObject
        */
        public static GameObject getObject(string gameObjectName) {
             return GameObject.Find(gameObjectName);
        }


        /*
        * オブジェクトの名前からゲームオブジェクトの位置情報を取得
        *
        */
        public static Vector3 position(string objectName) {
            return GameObject.Find(objectName).transform.position;
        }
    }
}

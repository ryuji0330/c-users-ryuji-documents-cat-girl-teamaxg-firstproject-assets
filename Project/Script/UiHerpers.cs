﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Constant;


namespace UiHelpers{

    public class SlotFlag : MonoBehaviour{

        public static string itemFrag = Item.NONE;
        public static Dictionary<string, string> flag = new Dictionary<string, string> { { Slot.POWER_UP_SLOT, Item.NONE }, { Slot.HEAL_SLOT, Item.NONE } };

        public static void Imagefunc(string ObjectName, string SpriteName)
        {
            GameObject Sloat = GameObject.Find(ObjectName);
            Image SloatImage = Sloat.GetComponent<Image>();
            Sprite sprite = Resources.Load<Sprite>(SpriteName);
            SloatImage.sprite = sprite;
            switch (SpriteName)
            {
                case Item.BOOMERANG_PANTS:
                    if (flag[Slot.POWER_UP_SLOT] != Item.NONE)
                    {
                        return;
                    }
                    flag[Slot.POWER_UP_SLOT] = Item.BOOMERANG_PANTS;
                    break;
                case Item.NEKOKAN:
                    if (flag[Slot.HEAL_SLOT] != Item.NONE)
                    {
                        return;
                    }
                    flag[Slot.HEAL_SLOT] = Item.NEKOKAN;
                    break;
            }
        }
    }

    public class ScoreNumber : MonoBehaviour
    {

        public static int score = 0;

        public static void NumberImage(string ScoreObject, int Number)
        {
            GameObject Sloat = GameObject.Find(ScoreObject);
            Image SloatImage = Sloat.GetComponent<Image>();
            Sprite[] sprite = Resources.LoadAll<Sprite>(Score.NUMBER);
            SloatImage.sprite = sprite[Number];
        }
    }

    public class ItemEffect : MonoBehaviour
    {
        public static IEnumerator coinGet(Vector3 coinPosition,int coinValue,string coinGetEffect)
        {
            coinPosition.y = coinPosition.y + 2f;
            GameObject effect = GameObject.Instantiate((GameObject)Resources.Load(coinGetEffect), coinPosition, Quaternion.identity);
            ScoreNumber.score += coinValue;

            // 5秒待つ  
            yield return new WaitForSeconds(3.0f);

            GameObject.Destroy(effect);
        }
    }
}
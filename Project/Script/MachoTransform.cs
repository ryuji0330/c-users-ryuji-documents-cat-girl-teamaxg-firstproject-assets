﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using Constant;
using UiHelpers;
using Pause;

public class MachoTransform : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {


    }


    // Update is called once per frame
    void Update()
    {
        
    }

    public void PowerUpSlotTouch()
    {
        Vector3 catPosition = GameObject.Find("cat_01").transform.position;
        GameObject.Destroy(GameObject.Find("cat_01"));
        GameObject.Instantiate((GameObject)Resources.Load("effects/machoTransform"), catPosition, Quaternion.Euler(-90, 90, 0));
        GameObject.Instantiate((GameObject)Resources.Load("prefabs/cat_02"), catPosition, Quaternion.Euler(0,90,0));
        SlotFlag.Imagefunc(Slot.POWER_UP_SLOT, Item.NONE);
        SlotFlag.flag[Slot.POWER_UP_SLOT] = Item.NONE;
    }
}

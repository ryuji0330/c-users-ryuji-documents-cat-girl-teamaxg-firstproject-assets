using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Constant;

public class MoveImage : MonoBehaviour {
    private const int CLOUD_COUNT = 4;

    private float init;
    private RectTransform[] cloudList = new RectTransform[CLOUD_COUNT];
    private Vector3[] cloudPosList    = new Vector3[CLOUD_COUNT];

    void Start(){
        cloudList = new RectTransform[CLOUD_COUNT] {
            GameObject.Find("cloud01").GetComponent<RectTransform>(),
            GameObject.Find("cloud02").GetComponent<RectTransform>(),
            GameObject.Find("cloud03").GetComponent<RectTransform>(),
            GameObject.Find("cloud04").GetComponent<RectTransform>()
        };

        cloudPosList = new Vector3[CLOUD_COUNT] {
            cloudList[0].localPosition,
            cloudList[1].localPosition,
            cloudList[2].localPosition,
            cloudList[3].localPosition
        };

        // 基準となる位置を取得
        init = cloudPosList[3].x;
    }

    void Update() {
        for(int i = 0; i < CLOUD_COUNT; i++) {
            cloudPosList[i].x -= 0.5f;
            cloudList[i].localPosition = cloudPosList[i];
        }

        this.MoveInitPos(cloudList, cloudPosList);
    }

    void MoveInitPos(RectTransform[] _cloud, Vector3[] _cloudPos) {
        for (int i = 0; i < CLOUD_COUNT; i++) {
            if (_cloud[i].localPosition.x <= Title.INIT_POS_X) {
                _cloudPos[i].x = init;
                _cloud[i].localPosition = new Vector3(init, _cloudPos[i].y, _cloudPos[i].z);
            }
        }
    }
}

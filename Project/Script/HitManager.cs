﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Constant;
using UiHelpers;
using HpHelpers;

public class HitManager : MonoBehaviour{
    /*public enum Item
    {
        nekokan,
        brief,
        bikini,
        nekojyarashi,
        milk,
        matatabi
    };*/

    /*public void Awake()
    {

        Slot1 = GameObject.Find("Slot1");
        Slot2 = GameObject.Find("Slot2");
        Slot3 = GameObject.Find("Slot3");

        Image SlotImage = Slot1.GetComponent<Image>();

        Image1 = Resources.Load<Sprite>("nekokan");

        SlotImage.sprite = Image1;
        this.Imagefunc("Slot1", "brief");
        this.Imagefunc("Slot2", "brief");
        this.Imagefunc("Slot3", "brief");

    }*/
    void Start(){
        ParticleSystem sCoinGet = GetComponent<ParticleSystem>();
        ParticleSystem.MainModule sCoinGetMain = sCoinGet.main;
        ParticleSystem.MinMaxCurve sCoinGetMinMaxCurve = sCoinGet.startSpeed;
    }

    void OnTriggerEnter(Collider hit){
        Debug.Log(hit.gameObject.tag);

        switch (hit.gameObject.tag){
            case Tag.BOOMERANG_PANTS:
                GameObject.Destroy(hit.gameObject);
                SlotFlag.Imagefunc(Slot.POWER_UP_SLOT, Item.BOOMERANG_PANTS);
                break;

            case Tag.NEKOKAN:
                GameObject.Destroy(hit.gameObject);
                SlotFlag.Imagefunc(Slot.HEAL_SLOT, Item.NEKOKAN);
                break;

            case Tag.G_COIN:
                Vector3 gCoinPosition = hit.gameObject.transform.position;
                StartCoroutine(ItemEffect.coinGet(gCoinPosition, Score.G_COIN_VALUE, "effects/G_coin_get"));
                GameObject.Destroy(hit.gameObject);
                break;

            case Tag.S_COIN:
                Vector3 sCoinPosition = hit.gameObject.transform.position;
                StartCoroutine(ItemEffect.coinGet(sCoinPosition, Score.G_COIN_VALUE, "effects/S_coin_get"));
                GameObject.Destroy(hit.gameObject);
                break;

            case Tag.DOG:
                ScoreNumber.score += Score.DOG_VALUE;
                break;

            case Tag.DOG_DAMAGE:
                HpManager.t -= 0.3f;
                break;
        }
    }


};



    /*public ItemData(Sprite image, string itemName)
    {
        this.itemSprite = image;
        this.itemName = itemName;

    }

    public Sprite GetItemSprite()
    {
        return itemSprite;
    }

    public string GetItemName()
    {
        return itemName;
    }*/



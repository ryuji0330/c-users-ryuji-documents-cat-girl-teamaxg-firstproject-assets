﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Constant;
using UnityEngine.SceneManagement;

public class SceneManageController : MonoBehaviour {

    // 現在のシーン名を格納
    private string nowScenes;
    public string currentScene;

    void Awake() {
        // シーンの遷移とフェードを管理するオブジェクトの作成
        GameObject Object = new GameObject();
        Object.AddComponent<SceneNavigator>();
        Object.AddComponent<Config>();

        GameObject SceneManagerObject = GameObject.Find("SceneManager");
        DontDestroyOnLoad(SceneManagerObject);
    }

    void Start() {
        SceneManager.activeSceneChanged += OnActiveSceneChanged;
        ActiveScene();
        Debug.Log(nowScenes);
    }

    void Update() {
        if (Input.GetMouseButtonDown(0)) {
            SceneNavigator.Instance.Change(nowScenes, 1f);
        }
        // 現在のシーン名を格納
        currentScene = SceneManager.GetActiveScene().name;
    }

    void OnActiveSceneChanged(Scene prevScene, Scene nextScene) {
        ActiveScene();
        Debug.Log(nowScenes);
    }

    // 現在のシーンを特定
    void ActiveScene() {
        switch(SceneManager.GetActiveScene().name) {
            case "op":
                nowScenes = Scenes.TITLE;
            break;
            case "title":
                nowScenes = Scenes.MAIN;
            break;
        }
    }

    // ムービーの終了を検知するコールバックメソッド
    bool loopPrevention = false; // ループ防止フラグ
    public void OnFinished() {
        if (!loopPrevention) {
            loopPrevention = true;
            return;
        }
        Debug.Log("OnFinished called!");
        SceneNavigator.Instance.Change(Scenes.TITLE, 1f);
    }
}

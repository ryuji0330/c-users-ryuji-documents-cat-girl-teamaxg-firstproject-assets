﻿using UnityEngine;
using UnityEngine.UI;
using EdText;
using System;

public class EdScroll : MonoBehaviour {
    private Text targetText;
    private float timeLimit;
    private void Start() {
        this.getEndingText();
    }

    private void Update() {
        transform.position += new Vector3(0, 100f * Time.deltaTime, 0);

        timeLimit += 1;
        Debug.Log(timeLimit);
        if (Input.GetMouseButtonDown (0) || timeLimit > 2000) {
            SceneNavigator.Instance.Change("title", 2);
        }
    }

    void getEndingText() {
        this.targetText = this.GetComponent<Text>();
        this.targetText.text = Ending.ROLL;
    }

}

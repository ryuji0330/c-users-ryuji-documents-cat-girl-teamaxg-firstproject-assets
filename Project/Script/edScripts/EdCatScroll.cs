﻿using UnityEngine;

public class EdCatScroll : MonoBehaviour {
    void Update() {
        transform.position += new Vector3(0, 1f * Time.deltaTime, 0);
        transform.Rotate(new Vector3(0, 1, 0));
    }
}

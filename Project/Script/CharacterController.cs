﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController : MonoBehaviour
{
    public float speed = 20;
    public Rigidbody rb;



    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        var moveHorizontal = Input.GetAxis("Horizontal");
        var moveVertical = Input.GetAxis("Vertical");
        var move = new Vector3(moveHorizontal, 0, moveVertical);

        rb.AddForce(move * speed);

        if (Input.GetKey(KeyCode.UpArrow))
        {
            Debug.Log("上");
            // transform.position += transform.forward * speed;

        }
        else if (Input.GetKey(KeyCode.LeftArrow))
        {
            Debug.Log("左");
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            Debug.Log("右");
        }
        else if (Input.GetKey(KeyCode.DownArrow))
        {
            Debug.Log("下");
            // transform.position -= transform.forward * speed;
        }


    }

    /*public void OnTriggerEnter(Collider other)
    {
        //もしもぶつかった相手に「Goal｝というTagがついていたら
        if (other.CompareTag("Goal"))
        {
            Debug.Log("Clear");

            Vector3 pos = this.gameObject.transform.position;
            this.gameObject.transform.position = new Vector3(pos.x = -6.5f, pos.y = 0.25f, pos.z = 6);
        }

        else if (other.CompareTag("Obstacle"))
        {
            Debug.Log("GameOver");

            Vector3 pos = this.gameObject.transform.position;
            this.gameObject.transform.position = new Vector3(pos.x = -6.5f, pos.y = 0.25f, pos.z = 6);
        }

        


        

    }*/


}

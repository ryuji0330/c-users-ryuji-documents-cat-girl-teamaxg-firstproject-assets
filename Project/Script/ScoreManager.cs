﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Constant;
using UiHelpers;

public class ScoreManager : MonoBehaviour
{


    // Start is called before the first frame update
    void Start()
    {
    }


    // Update is called once per frame
    void Update()
    {
        string scoreChar = ScoreNumber.score.ToString("D7");
        string scoreOneMillionChar = scoreChar.Substring(0, 1);
        string scoreOneHundredThousandChar = scoreChar.Substring(1, 1);
        string scoreTenThousandChar = scoreChar.Substring(2, 1);
        string scoreOneThousandChar = scoreChar.Substring(3, 1);
        string scoreOneHundredChar = scoreChar.Substring(4, 1);
        string scoreTenChar = scoreChar.Substring(5, 1);
        string scoreOneChar = scoreChar.Substring(6, 1);
        int scoreOneMillion = int.Parse(scoreOneMillionChar);
        int scoreOneHundredThousand = int.Parse(scoreOneHundredThousandChar);
        int scoreTenThousand = int.Parse(scoreTenThousandChar);
        int scoreOneThousand = int.Parse(scoreOneThousandChar);
        int scoreOneHundred = int.Parse(scoreOneHundredChar);
        int scoreTen = int.Parse(scoreTenChar);
        int scoreOne = int.Parse(scoreOneChar);
        ScoreNumber.NumberImage(Score.SCORE7, scoreOneMillion += 1);
        ScoreNumber.NumberImage(Score.SCORE6, scoreOneHundredThousand += 1);
        ScoreNumber.NumberImage(Score.SCORE5, scoreTenThousand += 1);
        ScoreNumber.NumberImage(Score.SCORE4, scoreOneThousand += 1);
        ScoreNumber.NumberImage(Score.SCORE3, scoreOneHundred += 1);
        ScoreNumber.NumberImage(Score.SCORE2, scoreTen += 1);
        ScoreNumber.NumberImage(Score.SCORE1, scoreOne += 1);

        /*SlotFlag.Imagefunc(Score.SCORE1, scoreOneMillion);
        SlotFlag.Imagefunc(Score.SCORE2, scoreOneHundredThousand);
        SlotFlag.Imagefunc(Score.SCORE3, scoreTenThousand);
        SlotFlag.Imagefunc(Score.SCORE4, scoreOneThousand);
        SlotFlag.Imagefunc(Score.SCORE5, scoreOneHundred);
        SlotFlag.Imagefunc(Score.SCORE6, scoreTen);
        SlotFlag.Imagefunc(Score.SCORE7, scoreOne);*/
    }
    void FixedUpdate()
    {
        /*ScoreNumber.score += 1;*/
    }
}

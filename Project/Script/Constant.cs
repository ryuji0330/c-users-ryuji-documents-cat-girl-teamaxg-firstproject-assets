using UnityEngine;
using System.Collections;

namespace Constant {
    // ゲーム全般
    public static class Common {
        public const int FRAME_RATE = 30;
        public const int MAX_JUMP_COUNT = 2;
    }

    // 背景
    public static class Scenes {
        public const int SCROLL_LEFT = -1;
        public const int SCROLL_RIGHT = 1;
        public const int BLOCK_COUNT = 40;

        // Scenes
        public const string OP     = "Opening";
        public const string TITLE  = "Title";
        public const string MENU   = "Menu";
        public const string MAIN   = "Main";
        public const string ED     = "Ending";
	}

    // サウンド
    public static class Sound {
        public const int BGM       = 0;
        public const int SE        = 1;
    }

    // ステージ
    public static class Stage {
        // Stage
        public const int OKINAWA   = 1;                  // 沖縄
        public const int KYUUSHUU  = 2;                  // 九州
        public const int SHIKOKU   = 3;                  // 四国       
        public const int HONSHUU   = 4;                  // 本州
        public const int HOKKAIDO  = 5;                  // 北海道

        // Mode
        public const int RPG       = 1;
        public const int ENDLESS   = 2;

        // Level
        public const int EASY      = 1;
        public const int NORMAL    = 2;
        public const int HARD      = 3;

        // BackGround
        public const float DEAD_LINE = -20f;
        public const float FALL_LINE = -10f;

        // Image
        public const string CLOUD = "cloud";
        public const string MNT = "mnt";
        public const string MNT_SNOW = "mntSnow";
        public const string BUILDING = "building";

        // Result
        public const string GAMEOVER = "Game Over";
        public const string CLEAR = "Clear!";        
    }

    // スピード
    public static class ScrollSpeed {
        public const float EASY    = 5f;
        public const float NORMAL  = 8f;
        public const float HARD    = 15f;
    }

    // モーダル
    public static class Modal {
        public const string CONTINUE = "ButtonContinue";
        public const string GIVEUP = "ButtonGiveUp";
    }


    public static class Title {
        public const int INIT_POS_X = -400;
    }

    // アイテム
    public static class Item {
        public const string NEKOKAN = "images/Nekokan";
        public const string BOOMERANG_PANTS = "images/B_pants";
        public const string NONE = "images/None";
    }
    // タグ
    public static class Tag{
        public const string NEKOKAN = "Nekokan";
        public const string BOOMERANG_PANTS = "B_pants";
        public const string G_COIN = "G_coin";
        public const string S_COIN = "S_coin";
        public const string DOG = "Dog";
        public const string DOG_DAMAGE = "Dog_damage";
    }
    // スロット
    public static class Slot {
        public const string HEAL_SLOT = "Heal_slot";
        public const string POWER_UP_SLOT = "Power_up_slot";
    }
    //HP
    public static class Hp{
        public const string HP_GAGE = "Hp_gage";
    }
    //一時停止
    public static class Pauser{
        public const string PAUSE = "Pause";
        public const string START = "Start";
        public const string STOP = "Stop";
    }
    //スコア
    public static class Score{
        public const string SCORE1 = "Score1";
        public const string SCORE2 = "Score2";
        public const string SCORE3 = "Score3";
        public const string SCORE4 = "Score4";
        public const string SCORE5 = "Score5";
        public const string SCORE6 = "Score6";
        public const string SCORE7 = "Score7";
        public const string NUMBER = "images/numbers";
        public const int G_COIN_VALUE = 100;
        public const int S_COIN_VALUE = 50;
        public const int DOG_VALUE = 200;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Constant;
using UiHelpers;
using System;

namespace Pause{

    public class PauseManager : MonoBehaviour{
        static List<PauseManager> targets = new List<PauseManager>();   // ポーズ対象のスクリプト
        Behaviour[] pauseBehavs = null; // ポーズ対象のコンポーネント

        // 初期化
        void Start()
        {
            // ポーズ対象に追加する
            targets.Add(this);
        }

        // 破棄されるとき
        void OnDestory()
        {
            // ポーズ対象から除外する
            targets.Remove(this);
        }

        // ポーズされたとき
        void OnPause()
        {
            if (pauseBehavs != null)
            {
                return;
            }

            // 有効なBehaviourを取得
            pauseBehavs = Array.FindAll(GetComponentsInChildren<Behaviour>(), (obj) => { return obj.enabled; });

            foreach (var com in pauseBehavs)
            {
                com.enabled = false;
            }
        }

        // ポーズ解除されたとき
        void OnResume()
        {
            if (pauseBehavs == null)
            {
                return;
            }

            // ポーズ前の状態にBehaviourの有効状態を復元
            foreach (var com in pauseBehavs)
            {
                com.enabled = true;
            }

            pauseBehavs = null;
        }

        // ポーズ
        public static void machoPause()
        {
            foreach (var obj in targets)
            {
                obj.OnPause();
            }
        }

        // ポーズ解除
        public static void machoResume()
        {
            foreach (var obj in targets)
            {
                obj.OnResume();
            }
        }

        public void gamePause(){
            if (Time.timeScale != 0){
                Time.timeScale = 0;
                SlotFlag.Imagefunc(Pauser.PAUSE, Pauser.START);
            }
            else{
                Time.timeScale = 1.0f;
                SlotFlag.Imagefunc(Pauser.PAUSE, Pauser.STOP);
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleManager : MonoBehaviour {
    private Animator animator;
    void Start() {
        animator = GetComponent<Animator>();
        animator.SetBool("gameMain", false);
    }
}

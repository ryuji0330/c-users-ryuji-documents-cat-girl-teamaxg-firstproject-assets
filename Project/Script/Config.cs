﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Constant;

public class Config : MonoBehaviour {

    void Awake() {
        // 垂直同期オフ
        QualitySettings.vSyncCount = 0;
        // ゲームのフレームレート
        Application.targetFrameRate = Common.FRAME_RATE;
    }
}

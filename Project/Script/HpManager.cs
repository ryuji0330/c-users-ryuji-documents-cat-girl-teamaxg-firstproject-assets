﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Constant;
using UiHelpers;

namespace HpHelpers
{
    public class HpManager : MonoBehaviour
    {

        public static float t;
        public static float maxValue;
        public static RectTransform hp;
        public static Color hpColor;

        GameObject healSlot;
        GameObject powerUpSlot;

        void Start(){
            hp = GameObject.Find(Hp.HP_GAGE).gameObject.GetComponent<RectTransform>();
            maxValue = hp.sizeDelta.x;
            t = 1f;

            healSlot = GameObject.Find(Slot.HEAL_SLOT);
            powerUpSlot = GameObject.Find(Slot.POWER_UP_SLOT);
        }
        void UpdateValue(float t){
            float x = Mathf.Lerp(0f, maxValue, t);
            hp.sizeDelta = new Vector2(x, hp.sizeDelta.y);
        }
        void FixedUpdate(){
            t -= 0.0005f;
            UpdateValue(t);
            if (t <= 0f){
                t = 1f;
            }
        }

        public void HealSlotTouch(){
            Debug.Log("kaihuku!");
            if (SlotFlag.flag[Slot.HEAL_SLOT] == Item.NEKOKAN){
                t += 0.3f;
                if (t >= 1f){
                    t = 1f;
                }
                SlotFlag.Imagefunc(Slot.HEAL_SLOT, Item.NONE);
                SlotFlag.flag[Slot.HEAL_SLOT] = Item.NONE;
            }
        }
        void Update(){
            if (t > 0.5){
                hpColor = GameObject.Find(Hp.HP_GAGE).gameObject.GetComponent<Image>().color = Color.green;
            }
            else if (0.5 >= t && t >= 0.2){
                hpColor = GameObject.Find(Hp.HP_GAGE).gameObject.GetComponent<Image>().color = Color.yellow;
            }
            else if (t < 0.2){
                hpColor = GameObject.Find(Hp.HP_GAGE).gameObject.GetComponent<Image>().color = Color.red;
            }
        }
    }
}


    

    

        /*
        main = mainCamera.GetComponent<Camera>();
        Vector3 mousePos = main.ScreenToWorldPoint(Input.mousePosition);
        Collider2D col = Physics2D.OverlapPoint(mousePos);

        if (Input.GetMouseButtonDown(0)){
            if (col != healSlot.GetComponent<Collider2D>()){
                return;
            }
            else if (col != healSlot.GetComponent<Collider2D>())
            {
                //タップされた時の処理
                Debug.Log("HP回復");
            }
            if (col != powerUpSlot.GetComponent<Collider2D>()){
                return;
            }
            else if (col != healSlot.GetComponent<Collider2D>())
            {
                //タップされた時の処理
                Debug.Log("HP回復");
            }
        }*/

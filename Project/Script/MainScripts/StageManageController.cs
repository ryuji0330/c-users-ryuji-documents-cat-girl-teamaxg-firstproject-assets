﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Constant;

public class StageManageController : MonoBehaviour {
    private GameObject StageManager;
    StageCreater script;
    private GameObject[] stage;
    private int courseCount;
    public int level;
    public int selectedMode;
    public int _stageName;
    public float speed;
    private float initX;
    private Vector3 initPosition;
    
    void Awake() {
        StageManager = GameObject.Find("StageManager");
        script = StageManager.GetComponent<StageCreater>();
    }
    
    void Start() {
        this.GameStart(Stage.RPG, Stage.OKINAWA, Stage.HARD);
    }


    /*
    *   メニュー画面から最初に呼ばれステージを設定する
    *
    *   @param integer $mode モード
    *          integer $stageName ステージ名
    *          integer $selectedLevel レベル
    *
    */
    public void GameStart(int mode, int selectedStage, int selectedLevel) {
        // RPGモード
        selectedMode = mode;
        _stageName = selectedStage;
        if (selectedMode == Stage.RPG) {
            stage = script.SetStage(_stageName);
        // エンドレスモード
        } else {
            stage = script.SetEndlessStage();
        }
        level = selectedLevel;
        // レベルによりスクロールスピード変更
        if (level == Stage.EASY) {
            speed = ScrollSpeed.EASY;
        } else if (level == Stage.NORMAL) {
            speed = ScrollSpeed.NORMAL;
        } else {
            speed = ScrollSpeed.HARD;
        }
        courseCount = stage.Length;
        for (int i = 0; i < courseCount; i++) {
            initPosition = new Vector3(i * 500f, 0f, 0f);
            Instantiate(stage[i], initPosition, Quaternion.identity);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Constant;

public class ButtonClick : MonoBehaviour {
    private GameObject buttonContinue;
    private GameObject buttonGiveup;
    private RaycastHit hit;
    private Camera mainCamera;
    private float distance = 100f;
    private string buttonName;

    public bool continueClicked;
    public bool giveUpClicked;

    void Awake() {
        mainCamera = Camera.main;
        buttonContinue = GameObject.Find("ButtonContinue");
        buttonGiveup = GameObject.Find("ButtonGiveup");
    }

    void Start() {
        continueClicked = false;
        giveUpClicked = false;
    }

    void Update() {
        if (Input.GetMouseButtonDown(0)) {
            Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, distance)) {
                buttonName = hit.collider.gameObject.name;
                if (buttonName == Modal.CONTINUE) {
                    continueClicked = true;
                    giveUpClicked = false;
                } else if (buttonName == Modal.GIVEUP) {
                    giveUpClicked = true;
                    continueClicked = false;
                }
            }
        }
    }
}

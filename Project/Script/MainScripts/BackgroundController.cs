﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Constant;

public class BackgroundController : MonoBehaviour {
    private GameObject StageManager;
    StageManageController StageManageController;
    private int stageName;
    private GameObject Player;
    private GameObject bkg;
    private GameObject mnt01;
    private GameObject mnt02;
    private GameObject mnt03;
    private GameObject mntSnow01;
    private GameObject mntSnow02;
    private GameObject mntSnow03;
    private GameObject building_01;
    private GameObject building_02;
    private GameObject building_03;
    private GameObject building_04;
    private GameObject cloud01;
    private GameObject cloud02;
    private GameObject cloud03;
    private int imageCount;
    private GameObject[] cloudList;
    private GameObject[] addList;
    private GameObject[] _addList;
    private GameObject[] mntList;
    private GameObject[] mntSnowList;
    private GameObject[] bldList;
    private GameObject[] imgList;
    private Vector3 initPosition;
    private float posX;
    private float posY;
    private float posZ;
    private float initX;
    private float initY;
    private float initZ;
    private Vector3 imgPosList;
    private int listCount;

    void Awake() {
        StageManager = GameObject.Find("StageManager");
        StageManageController = StageManager.GetComponent<StageManageController>();
        // 背景
        bkg = (GameObject)Resources.Load("Prefabs/BackGround");

        // 山
        mnt01 = (GameObject)Resources.Load("Prefabs/mnt01");
        mnt02 = (GameObject)Resources.Load("Prefabs/mnt02");
        mnt03 = (GameObject)Resources.Load("Prefabs/mnt03");
        
        // 雪山
        mntSnow01 = (GameObject)Resources.Load("Prefabs/mntSnow01");
        mntSnow02 = (GameObject)Resources.Load("Prefabs/mntSnow02");
        mntSnow03 = (GameObject)Resources.Load("Prefabs/mntSnow03");

        // ビル
        building_01 = (GameObject)Resources.Load("Prefabs/building_01");
        building_02 = (GameObject)Resources.Load("Prefabs/building_02");
        building_03 = (GameObject)Resources.Load("Prefabs/building_03");
        building_04 = (GameObject)Resources.Load("Prefabs/building_04");

        // 雲
        cloud01 = (GameObject)Resources.Load("Prefabs/cloud01");
        cloud02 = (GameObject)Resources.Load("Prefabs/cloud02");
        cloud03 = (GameObject)Resources.Load("Prefabs/cloud03");        
    }

    void Start() {
        mntList = new GameObject[] {
            mnt01,
            mnt02,
            mnt03
        };
        mntSnowList = new GameObject[] {
            mntSnow01,
            mntSnow02,
            mntSnow03
        };
        bldList = new GameObject[] {
            building_01,
            building_02,
            building_03,
            building_04
        };
        cloudList = new GameObject[] {
            cloud01,
            cloud02,
            cloud03
        };
        stageName = StageManageController._stageName;
        addList = this.SetBackground(stageName);
        imgList = new GameObject[cloudList.Length + addList.Length];
        cloudList.CopyTo(imgList, 0);
        addList.CopyTo(imgList, cloudList.Length);
        imageCount = imgList.Length;

        // 背景のパネル生成
        Instantiate(bkg, new Vector3(10f, 10f, 50f), Quaternion.Euler(-90f, 0, 0));
        foreach (GameObject img in imgList) {
            string imgName = img.name;
            switch (imgName) {
                // 雲
                case "cloud01":
                    posX = 65f;
                    posY = 28f;
                    posZ = 14f;
                    break;
                case "cloud02":
                    posX = 100f;
                    posY = 27f;
                    posZ = 13f;
                    break;
                case "cloud03":
                    posX = 150f;
                    posY = 26f;
                    posZ = 12f;
                    break;
                // 山
                case "mnt01":
                    posX = 90f;
                    posY = 6f;
                    posZ = 15f;
                    break;
                case "mnt02":
                    posX = 150f;
                    posY = 4f;
                    posZ = 16f;
                    break;
                case "mnt03":
                    posX = 220f;
                    posY = 5f;
                    posZ = 17f;
                    break;
                case "mntSnow01":
                    posX = 90f;
                    posY = 7.3f;
                    posZ = 15f;
                    break;
                case "mntSnow02":
                    posX = 150f;
                    posY = 8.6f;
                    posZ = 16f;
                    break;
                case "mntSnow03":
                    posX = 220f;
                    posY = 9.3f;
                    posZ = 17f;
                    break;
                case "building_01":
                    posX = 80f;
                    posY = 13f;
                    posZ = 15f;
                    break;
                case "building_02":
                    posX = 110f;
                    posY = 13f;
                    posZ = 16f;
                    break;
                case "building_03":
                    posX = 120f;
                    posY = 10f;
                    posZ = 17f;
                    break;
                case "building_04":
                    posX = 150f;
                    posY = 12f;
                    posZ = 18f;
                    break;
                default:
                    break;
            }
            imgPosList = new Vector3(posX, posY, posZ);
            Instantiate(img, imgPosList, Quaternion.Euler(-90f, 0, 0));
        }
    }

    private GameObject[] SetBackground(int _stageName) {
        switch (_stageName) {
            case Stage.OKINAWA:
            case Stage.KYUUSHUU:
            case Stage.SHIKOKU:
                _addList = mntList;
                break;
            case Stage.HONSHUU:
                _addList = bldList;
                break;
            case Stage.HOKKAIDO:
                _addList = mntSnowList;
                break;
            default:
                break;
        }
        return _addList;
    }
}

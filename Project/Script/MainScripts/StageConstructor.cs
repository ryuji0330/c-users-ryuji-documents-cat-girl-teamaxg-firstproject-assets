﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Constant;

public class StageConstructor : MonoBehaviour
{
    private GameObject[] stage;
    private GameObject[] st_01;
    private GameObject[] st_02;
    private GameObject[] st_03;
    private GameObject[] st_04;
    private GameObject[] st_05;
    private GameObject[] st_endless; 

    // コース
    // 沖縄01 
    private GameObject C_1_01;
    // 沖縄02
    private GameObject C_1_02;

    // 九州01
    private GameObject C_2_01;
    // 九州02
    private GameObject C_2_02;

    // 四国01
    private GameObject C_3_01;
    // 四国02
    private GameObject C_3_02;
    // 四国03
    private GameObject C_3_03;

    // 本州01
    private GameObject C_4_01;
    // 本州02
    private GameObject C_4_02;
    // 本州03
    private GameObject C_4_03;
    // 本州04
    private GameObject C_4_04;

    // 北海道01
    private GameObject C_5_01;
    // 北海道02
    private GameObject C_5_02;
    // 北海道03
    private GameObject C_5_03;
    // 北海道04
    private GameObject C_5_04;

    void Awake() {
        // 沖縄
        C_1_01 = (GameObject)Resources.Load("Prefabs/C_1-01");
        C_1_02 = (GameObject)Resources.Load("Prefabs/C_1-02");
        // 九州
        C_2_01 = (GameObject)Resources.Load("Prefabs/C_2-01");
        C_2_02 = (GameObject)Resources.Load("Prefabs/C_2-02");
        // 四国
        C_3_01 = (GameObject)Resources.Load("Prefabs/C_3-01");
        C_3_02 = (GameObject)Resources.Load("Prefabs/C_3-02");
        C_3_03 = (GameObject)Resources.Load("Prefabs/C_3-03");
        // 本州
        C_4_01 = (GameObject)Resources.Load("Prefabs/C_4-01");
        C_4_02 = (GameObject)Resources.Load("Prefabs/C_4-02");
        C_4_03 = (GameObject)Resources.Load("Prefabs/C_4-03");
        C_4_04 = (GameObject)Resources.Load("Prefabs/C_4-04");
        // 北海道
        C_5_01 = (GameObject)Resources.Load("Prefabs/C_5-01");
        C_5_02 = (GameObject)Resources.Load("Prefabs/C_5-02");
        C_5_03 = (GameObject)Resources.Load("Prefabs/C_5-03");
        C_5_04 = (GameObject)Resources.Load("Prefabs/C_5-04"); 
        // ステージ
        // 沖縄
        st_01 = new GameObject[] {C_1_01, C_1_02};
        // 九州
        st_02 = new GameObject[] {C_2_01, C_2_02};
        // 四国
        st_03 = new GameObject[] {C_3_01, C_3_02, C_3_03};
        // 本州
        st_04 = new GameObject[] {C_4_01, C_4_02, C_4_03, C_4_04};
        // 北海道
        st_05 = new GameObject[] {C_5_01, C_5_02, C_5_03, C_5_04};
        // エンドレス
        st_endless = new GameObject[] {
            C_1_01, C_1_02, C_2_01, C_2_02, C_3_01, C_3_02, C_3_03, C_4_01, C_4_02, C_4_03, C_4_04, C_5_01, C_5_02, C_5_03, C_5_04
        };
    }

    public GameObject[] GetRpgStage (int stageName) {
        switch (stageName) {
            case Stage.OKINAWA:
                stage = st_01;
                break;
            case Stage.KYUUSHUU:
                stage = st_02;
                break;
            case Stage.SHIKOKU:
                stage = st_03;
                break;
            case Stage.HONSHUU:
                stage = st_04;
                break;
            case Stage.HOKKAIDO:
                stage = st_05;
                break;
            default:
                break;
        }
        return stage;
    }

    public GameObject[] GetEndlessStage () {
        return st_endless;
    }
}

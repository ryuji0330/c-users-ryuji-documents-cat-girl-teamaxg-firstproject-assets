﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Constant;

public class ScrollStage : MonoBehaviour {
    private GameObject StageManager;
    StageManageController StageManageController;
    GameController GameController;
    private const int DIRECTION = Scenes.SCROLL_LEFT;
    private bool gameOver;
    private float scrollSpeed;
    private float positionY = 0f;
    private float positionZ = 0f;
    private float vertical;
    private float deadEnd = -500f;

    void Awake() {
        StageManager = GameObject.Find("StageManager");
        StageManageController = StageManager.GetComponent<StageManageController>();
        GameController = StageManager.GetComponent<GameController>();
    }

    void Start() {
        scrollSpeed = StageManageController.speed;
    }

    void Update() {
        gameOver = GameController.gameOver;
        // HPが0もしくは穴に落ちたらスクロール停止
        if (gameOver) {
            transform.Translate(0, 0, 0);
        } else {
            transform.position += new Vector3(DIRECTION * scrollSpeed * Time.deltaTime, positionY, positionZ);
            if (transform.position.x <= deadEnd) {
                Destroy(this.gameObject);
            }
        }
    }
}

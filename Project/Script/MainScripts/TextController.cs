﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Constant;

public class TextController : MonoBehaviour {
    private GameObject StageManager;
    GameController GameController;
    private Text resultText;
    private bool gameOver;

    void Awake() {
        StageManager = GameObject.Find("StageManager");
        GameController = StageManager.GetComponent<GameController>();
    }

    void Start() {
        
    }

    void Update() {
        gameOver = GameController.gameOver;
        if (gameOver) {
            this.ShowResult();
        }
    }

    void ShowResult() {
        this.resultText = this.GetComponent<Text>();
        this.resultText.text = Stage.GAMEOVER;
    }
}

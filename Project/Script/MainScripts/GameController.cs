﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Constant;

public class GameController : MonoBehaviour {
    private GameObject Player;
    private float vertical;
    private bool isDead;
    private bool isClear;
    private bool continueClicked;
    private bool giveUpClicked;
    private GameObject cat_01;
    private GameObject DialogPanel;
    ButtonClick ButtonClick;

    public bool isFalled;
    public bool gameOver;

    void Awake() {
        Player = GameObject.FindGameObjectWithTag("Player");
        cat_01 = (GameObject)Resources.Load("cat_01");
        DialogPanel = (GameObject)Resources.Load("Prefabs/DialogPanel");
        ButtonClick = DialogPanel.GetComponent<ButtonClick>();
    }

    void Start() {
        isDead = false;
        isFalled = false;
        isClear = false;
        gameOver = false;
    }

    void Update() {
        // 猫が穴に落ちた時
        vertical = Player.transform.position.y;
        if (vertical < Stage.FALL_LINE) {
            isFalled = true;
        }

        // ダイアログ
        continueClicked = ButtonClick.continueClicked;
        giveUpClicked = ButtonClick.giveUpClicked; 

        // HPが0になった時
        //if (remainingHP == 0) {
        //    isDead = true;
        //}

        if (isFalled || isDead || isClear) {
            gameOver = true;
            Instantiate(DialogPanel, new Vector3(14f, 8.2f, -15f), Quaternion.Euler(-90f, 0, 0));
            if (continueClicked) {
                Destroy(DialogPanel);
                Destroy(Player);
                Instantiate(cat_01, new Vector3(0f, 25f, -5f), Quaternion.identity);
                gameOver = false;
                isFalled = false;
                return;
            } else if (giveUpClicked) {
                Destroy(DialogPanel);
                return;
            }
        }
    }
}

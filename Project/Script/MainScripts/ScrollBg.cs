﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Constant;

public class ScrollBg : MonoBehaviour {
    private const int DIRECTION = Scenes.SCROLL_LEFT;

    private GameObject StageManager;
    StageManageController StageManageController;
    GameController GameController;
    private bool gameOver;
    private float mntSpeed;
    private float bldSpeed;
    private float cloudSpeed;
    private int level;
    private float mag;
    private string tagName;
    private float vecX;
    private float deadEnd = -50f;
    private float initX = 70f;
    private float initY;
    private float initZ;
    private float vertical;

    void Awake() {
        StageManager = GameObject.Find("StageManager");
        StageManageController = StageManager.GetComponent<StageManageController>();
        GameController = StageManager.GetComponent<GameController>();
        level = StageManageController.level;
    }

    void Start() {
        if (level == Stage.EASY) {
            mag = 0.8f;
        } else if (level == Stage.NORMAL) {
            mag = 1f;
        } else {
            mag = 1.8f;
        }
        tagName = transform.tag;
        initY = transform.position.y;
        initZ = transform.position.z;
        mntSpeed = mag * DIRECTION * 0.8f;
        bldSpeed = mag * DIRECTION * 1.5f;
        cloudSpeed = mag * DIRECTION * 1.2f;
    }


    void Update() {
        gameOver = GameController.gameOver;
        // HPが0もしくは穴に落ちたらスクロール停止
        if (gameOver) {
            transform.Translate(0, 0, 0);
        } else {
            if (tagName == Stage.CLOUD) {
                vecX = cloudSpeed * Time.deltaTime;
                initY = Random.Range(24f, 28f);
            } else if (tagName == Stage.MNT || tagName == Stage.MNT_SNOW) {
                vecX = mntSpeed * Time.deltaTime;
            } else if (tagName == Stage.BUILDING) {
                vecX = bldSpeed * Time.deltaTime;
            }
            transform.position += new Vector3(vecX, 0f, 0f);

            if (transform.position.x <= deadEnd) {
                transform.position = new Vector3(initX, initY, initZ);
            }
        }
    }
}

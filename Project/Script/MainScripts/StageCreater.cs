﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Constant;

public class StageCreater : MonoBehaviour {
    private GameObject StageManager;
    StageConstructor script;
    private const int DIRECTION = Scenes.SCROLL_LEFT;
    private GameObject[] stage;
    private Vector3 initPosition;
    private float speed;
    private int courseCount;

    void Awake() {
        StageManager = GameObject.Find("StageManager");
        script = StageManager.GetComponent<StageConstructor>();
    }

    public GameObject[] SetStage(int stageName) {
        stage = script.GetRpgStage(stageName);
        return stage;
    }

    public GameObject[] SetEndlessStage() {
        stage = script.GetEndlessStage();
        return stage;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Constant;

public class EnemyMotion : MonoBehaviour {
    private const int DIRECTION = Scenes.SCROLL_LEFT;
    private const float SHOW_LINE = 45f;

    private GameObject StageManager;
    StageManageController StageManageController;
    private GameObject JukeBoxObject;
    JukeBox JukeBox;
    private Animator animator;
    private int level;
    private float mag;
    private float stageSpeed;
    private float dogSpeed;
    private float vertical;
    private float timeElapsed;

    void Awake() {
        StageManager = GameObject.Find("StageManager");
        StageManageController = StageManager.GetComponent<StageManageController>();
        JukeBoxObject = GameObject.Find("JukeBox");
        JukeBox = JukeBoxObject.GetComponent<JukeBox>();
        level = StageManageController.level;
    }

    void Start() {
        // ステージのスクロールスピード
        stageSpeed = StageManageController.speed;
        // レベルにより倍率設定
        if (level == Stage.EASY) {
            mag = 1.1f;
        } else if (level == Stage.NORMAL) {
            mag = 1.5f;
        } else {
            mag = 2.5f;
        }
        // 犬のスピード
        dogSpeed = mag * DIRECTION * stageSpeed;
    }


    void Update() {
        if (transform.position.x <= SHOW_LINE && transform.position.x >= Stage.DEAD_LINE) {
            BowBow();
            transform.position += new Vector3(dogSpeed * Time.deltaTime, 0f, 0f);
        } else if (transform.position.x <= Stage.DEAD_LINE) {
            Destroy(this.gameObject);
        }

    }

     void OnCollisionEnter (Collision col) {
        if (col.gameObject.tag == "Player") {
            Destroy(this.gameObject);
        }
     }

     void BowBow() {
        timeElapsed += Time.deltaTime;
        for (int i = 1; i < 10; i++) {
            JukeBox.Bow();
        }
     }
}

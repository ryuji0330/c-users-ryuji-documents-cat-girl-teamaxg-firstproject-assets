﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Constant;

public class JukeBox : MonoBehaviour {
    private const int BGM = 0;
    private const int SE_CAT = 1;
    private const int SE_DOG = 2;
    private GameObject Player;
    private float vertical;
    private GameObject StageManager;
    StageManageController StageManageController;
    private int mode;
    private int stageName;
    private int level;
    private string currentScene;
    private AudioSource bgm;
    private AudioSource cat;
    private AudioSource dog;

    private AudioSource[] soundTrack;
    private AudioClip traseform_01;
    private AudioClip st_01_01;
    private AudioClip st_01_02;
    private AudioClip st_01_03;
    private AudioClip st_02_01;
    private AudioClip st_02_02;
    private AudioClip st_02_03;
    private AudioClip st_03_01;
    private AudioClip st_03_02;
    private AudioClip st_03_03;
    private AudioClip st_04_01;
    private AudioClip st_04_02;
    private AudioClip st_04_03;
    private AudioClip st_05_01;
    private AudioClip st_05_02;
    private AudioClip st_05_03;
    private AudioClip cat_01;
    private AudioClip cat_02;
    private AudioClip dog_04;

    void Awake() {
        soundTrack = gameObject.GetComponents<AudioSource>();
        bgm = soundTrack[BGM];
        cat = soundTrack[SE_CAT];
        dog = soundTrack[SE_DOG];
        StageManager = GameObject.Find("StageManager");
        StageManageController = StageManager.GetComponent<StageManageController>();
        // オーディオファイル読み込み
        traseform_01 = (AudioClip)Resources.Load("sounds/BGM/traseform_01");
        st_01_01 = (AudioClip)Resources.Load("sounds/BGM/st_01_01");
        st_01_02 = (AudioClip)Resources.Load("sounds/BGM/st_01_02");
        st_01_03 = (AudioClip)Resources.Load("sounds/BGM/st_01_03");
        st_02_01 = (AudioClip)Resources.Load("sounds/BGM/st_02_01");
        st_02_02 = (AudioClip)Resources.Load("sounds/BGM/st_02_02");
        st_02_03 = (AudioClip)Resources.Load("sounds/BGM/st_02_03");
        st_03_01 = (AudioClip)Resources.Load("sounds/BGM/st_03_01");
        st_03_02 = (AudioClip)Resources.Load("sounds/BGM/st_03_02");
        st_03_03 = (AudioClip)Resources.Load("sounds/BGM/st_03_03");
        st_04_01 = (AudioClip)Resources.Load("sounds/BGM/st_04_01");
        st_04_02 = (AudioClip)Resources.Load("sounds/BGM/st_04_02");
        st_04_03 = (AudioClip)Resources.Load("sounds/BGM/st_04_03");
        st_05_01 = (AudioClip)Resources.Load("sounds/BGM/st_05_01");
        st_05_02 = (AudioClip)Resources.Load("sounds/BGM/st_05_02");
        st_05_03 = (AudioClip)Resources.Load("sounds/BGM/st_05_03");
        cat_01 = (AudioClip)Resources.Load("sounds/SE/cat_01");
        cat_02 = (AudioClip)Resources.Load("sounds/SE/cat_02");
        dog_04 = (AudioClip)Resources.Load("sounds/SE/dog_04");
    }

    void Start() {
        mode = StageManageController.selectedMode;
        stageName = StageManageController._stageName;
        level = StageManageController.level;
        this.SelectSound(stageName, level);
        bgm.Play();
    }

    void Update() {
    }


    // ステージとレベルにより曲を選択
    void SelectSound(int stage, int level) {
        switch (stage) {
            case Stage.OKINAWA:
                if (level == Stage.EASY) {
                    bgm.clip = st_01_01;
                } else if (level == Stage.NORMAL) {
                    bgm.clip = st_01_02;
                } else {
                    bgm.clip = st_01_03;
                }
                break;
            case Stage.KYUUSHUU:
                if (level == Stage.EASY) {
                    bgm.clip = st_02_01;
                } else if (level == Stage.NORMAL) {
                    bgm.clip = st_02_02;
                } else {
                    bgm.clip = st_02_03;
                }
                break;
            case Stage.SHIKOKU:
                if (level == Stage.EASY) {
                    bgm.clip = st_03_01;
                } else if (level == Stage.NORMAL) {
                    bgm.clip = st_03_02;
                } else {
                    bgm.clip = st_03_03;
                }
                break;
            case Stage.HONSHUU:
                if (level == Stage.EASY) {
                    bgm.clip = st_04_01;
                } else if (level == Stage.NORMAL) {
                    bgm.clip = st_04_02;
                } else {
                    bgm.clip = st_04_03;
                }
                break;
            case Stage.HOKKAIDO:
                if (level == Stage.EASY) {
                    bgm.clip = st_05_01;
                } else if (level == Stage.NORMAL) {
                    bgm.clip = st_05_02;
                } else {
                    bgm.clip = st_05_03;
                }
                break;
            default:
                break;
        }
    }

    // ジャンプするときの声
    public void Mew() {
        cat.PlayOneShot(cat_01);
    }

    // ダメージ受けた時の声
    public void damagedCry() {
        cat.PlayOneShot(cat_02);
    }

    // 犬の鳴き声
    public void Bow() {
        dog.PlayOneShot(dog_04);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Constant;

public class PlayerCharacterController : MonoBehaviour {

    public float speed     = 0.15f;
    public float jumpPower = 5.0f;
    private int jumpCount  = 0;
    private bool isJump = false;
    private GameObject JukeBoxObject;
    JukeBox JukeBox;
    private Rigidbody rb;
    private Animator animator;

    void Awake() {
        JukeBoxObject = GameObject.Find("JukeBox");
        JukeBox = JukeBoxObject.GetComponent<JukeBox>();
    }

    void Start() {
        rb = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();
        animator.SetBool("gameMain", true);
    }

    void Update() {
        Move();
        Jump();
    }

    void Move() {
        float horizontal = Input.GetAxis("Horizontal");
        transform.Translate(new Vector3(0, 0, horizontal * speed));
    }

    void Jump() {
        if (Input.GetKeyDown("space") && jumpCount < Common.MAX_JUMP_COUNT) {
            JukeBox.Mew();
            if (jumpCount == 1) {
                animator.SetBool("jumpFlg", false);
                animator.SetBool("doubleJumpFlg", true);
                isJump = true;
                return;
            }

            animator.SetBool("jumpFlg", true);
            animator.SetBool("doubleJumpFlg", false);
            isJump = true;
        }
    }

    void FixedUpdate() {
        if (isJump) {
            // 速度をクリアして2回目のジャンプも1回目と同じ挙動にする。
            rb.velocity = Vector3.zero;
            rb.AddForce(Vector3.up * jumpPower, ForceMode.Impulse);
            jumpCount++;
            isJump = false;
        }
    }


    void OnCollisionEnter(Collision collision) {
        if (collision.gameObject.tag == "Ground") {
            jumpCount = 0;
            animator.SetBool("jumpFlg", false);
            animator.SetBool("doubleJumpFlg", false);
        }
    }

    void OnTriggerEnter (Collider other) {
        if (other.gameObject.tag == "Enemy" || other.gameObject.tag == "Block") {
            JukeBox.damagedCry();
            animator.SetTrigger("damageTrigger");
            StartCoroutine ("Blink");
        }
    }

    IEnumerator Blink() {
        Debug.Log("ダメージ");
        StartCoroutine ("StopBlink");
        while (true) {
            var renderComponent = transform.GetChild(0).GetComponent<SkinnedMeshRenderer>();
            // var renderComponent = transform.FindChild("cat_01/cat_01").GetComponent<SkinnedMeshRenderer>();
            renderComponent.enabled = !renderComponent.enabled;
            yield return new WaitForSeconds(0.15f);
        }
    }

    IEnumerator StopBlink() {
        yield return new WaitForSeconds(2.0f);
        StopCoroutine("Blink");
        transform.GetChild(0).GetComponent<SkinnedMeshRenderer>().enabled = true;
        // transform.FindChild("cat_01/cat_01").GetComponent<SkinnedMeshRenderer>().enabled = true;
    }
}
